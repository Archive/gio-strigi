/* This file is part of Strigi Desktop Search
 *
 * Copyright (C) 2009 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#include "strigi-gio-inputstream.h"

using namespace Tracker;

GioInputStream::GioInputStream(GInputStream* input)
        :p((Private*)input) {
    // The private member would only contain a pointer to an input object,
    // so we avoid an allocation by casting GInputStream to it.
}
GioInputStream::~GioInputStream() {
}
/**
 * Implementation of abstract virtual function from Strigi::BufferedStream.
 * See Strigi::BufferedStream for documentation.
 **/
int32_t
GioInputStream::fillBuffer(char* start, int32_t space) {
    GInputStream* const g((GInputStream*)p);
    GError* error = NULL;
    gsize nread = g_input_stream_read(g, start, space, 0, &error);
    if (error != NULL) {
        m_status = Strigi::Error;
        m_error.assign(error->message);
        g_error_free(error);
        return -1;
    }
    return nread;
}
