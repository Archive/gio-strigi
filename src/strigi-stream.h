/* This file is part of Strigi Desktop Search
 *
 * Copyright (C) 2009 Philip Van Hoof <philip@codeminded.be>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the licence or (at
 * your option) any later version.
 *
 * See the included COPYING file for more information.
 *
 */

#ifndef _strigistream_h_
#define _strigistream_h_

#include <gio/gio.h>

G_BEGIN_DECLS

#define STRIGI_TYPE_STREAM                          (strigi_stream_get_type ())
#define STRIGI_STREAM(inst)                         (G_TYPE_CHECK_INSTANCE_CAST ((inst),                     \
                                                     STRIGI_TYPE_STREAM, StrigiStream))
#define STRIGI_STREAM_CLASS(class)                  (G_TYPE_CHECK_CLASS_CAST ((class),                       \
                                                     STRIGI_TYPE_STREAM, StrigiStreamClass))
#define STRIGI_IS_STREAM(inst)                      (G_TYPE_CHECK_INSTANCE_TYPE ((inst),                     \
                                                     STRIGI_TYPE_STREAM))
#define STRIGI_IS_STREAM_CLASS(class)               (G_TYPE_CHECK_CLASS_TYPE ((class),                       \
                                                     STRIGI_TYPE_STREAM))
#define STRIGI_STREAM_GET_CLASS(inst)               (G_TYPE_INSTANCE_GET_CLASS ((inst),                      \
                                                     STRIGI_TYPE_STREAM, StrigiStreamClass))

typedef struct _StrigiStreamPrivate                 StrigiStreamPrivate;
typedef struct _StrigiStreamClass                   StrigiStreamClass;
typedef struct _StrigiStream                        StrigiStream;

struct _StrigiStreamClass
{
  GInputStreamClass parent_class;
};

struct _StrigiStream
{
  GInputStream parent_instance;
  StrigiStreamPrivate *priv;
};

GType          strigi_stream_get_type (void);
GInputStream*  strigi_stream_new      (void *cppobj);

G_END_DECLS

#endif /* _strigistream_h_ */
