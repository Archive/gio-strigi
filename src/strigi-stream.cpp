/* This file is part of Strigi Desktop Search
 *
 * Copyright (C) 2009 Philip Van Hoof <philip@codeminded.be>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 */

#include <strigi/bufferedstream.h>

#include "strigi-stream.h"


G_DEFINE_TYPE (StrigiStream, strigi_stream, G_TYPE_INPUT_STREAM);

enum
{
  PROP_0,
  PROP_CPPOBJ
};

struct _StrigiStreamPrivate
{
  Strigi::InputStream *cppobj;
  /* pending operation metadata */
  GSimpleAsyncResult *result;
  GCancellable *cancellable;
  GMutex *lock;
  gpointer buffer;
  gsize count;
  gssize pos;
};

static void
strigi_stream_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  StrigiStream *stream = STRIGI_STREAM (object);

  switch (prop_id)
    {
      case PROP_CPPOBJ:
        g_value_set_pointer (value, stream->priv->cppobj);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
strigi_stream_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  StrigiStream *stream = STRIGI_STREAM (object);

  switch (prop_id)
    {
      case PROP_CPPOBJ:
        stream->priv->cppobj = (Strigi::InputStream *) g_value_get_pointer (value);
        break;

      default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
strigi_stream_finalize (GObject *object)
{
  StrigiStream *stream = STRIGI_STREAM (object);

  if (stream->priv->cppobj)
    delete stream->priv->cppobj;

  g_mutex_free (stream->priv->lock);

  if (G_OBJECT_CLASS (strigi_stream_parent_class)->finalize)
    (*G_OBJECT_CLASS (strigi_stream_parent_class)->finalize) (object);
}

static gssize 
cpp_read (StrigiStream *stream,
		  void *buffer, gsize count, 
		  GCancellable *cancellable, GError **error)
{
   Strigi::InputStream *cppobj = stream->priv->cppobj;
   const char* d;
   int32_t n = cppobj->read(d, stream->priv->pos, count);
   memcpy (buffer, d, count);
   stream->priv->pos += n;
}

static gssize
strigi_stream_read (GInputStream  *stream,
                            void          *buffer,
                            gsize          count,
                            GCancellable  *cancellable,
                            GError       **error)
{
  StrigiStream *input_stream = STRIGI_STREAM (stream);
  gssize ret;

  g_mutex_lock (input_stream->priv->lock);
  cpp_read (input_stream, buffer, count, cancellable, error);
  g_mutex_unlock (input_stream->priv->lock);

  return ret;
}

static gpointer
strigi_stream_read_ready (gpointer data)
{
  StrigiStream *stream = (StrigiStream *) data;
  GSimpleAsyncResult *simple;
  GError *error = NULL;

  simple = stream->priv->result;
  stream->priv->result = NULL;

  if (!g_cancellable_set_error_if_cancelled (stream->priv->cancellable,
                                             &error))
    {
      gssize result;

      g_mutex_lock (stream->priv->lock);
      cpp_read (stream, stream->priv->buffer, 
                stream->priv->count, 
                stream->priv->cancellable, &error);
      g_mutex_unlock (stream->priv->lock);

      if (result >= 0)
        g_simple_async_result_set_op_res_gssize (simple, result);
    }

  if (error)
    {
      g_simple_async_result_set_from_error (simple, error);
      g_error_free (error);
    }

  if (stream->priv->cancellable)
    g_object_unref (stream->priv->cancellable);

  g_simple_async_result_complete_in_idle (simple);

  g_object_unref (simple);

  return NULL;
}


static void
strigi_stream_read_async (GInputStream        *stream,
                                   void                *buffer,
                                   gsize                count,
                                   gint                 io_priority,
                                   GCancellable        *cancellable,
                                   GAsyncReadyCallback  callback,
                                   gpointer             user_data)
{
  StrigiStream *input_stream = STRIGI_STREAM (stream);
  GThread *thread;

  g_assert (input_stream->priv->result == NULL);

  input_stream->priv->result =
    g_simple_async_result_new (G_OBJECT (stream), callback, user_data,
                               (gpointer) strigi_stream_read_ready);

  if (cancellable)
    g_object_ref (cancellable);

  input_stream->priv->cancellable = cancellable;
  input_stream->priv->buffer = buffer;
  input_stream->priv->count = count;

  thread = g_thread_create (strigi_stream_read_ready, 
                            stream,
                            FALSE, NULL);  

}

static gssize
strigi_stream_read_finish (GInputStream  *stream,
                                    GAsyncResult  *result,
                                    GError       **error)
{
  GSimpleAsyncResult *simple;
  gssize count;

  g_return_val_if_fail (STRIGI_IS_STREAM (stream), -1);

  simple = G_SIMPLE_ASYNC_RESULT (result);

  g_warn_if_fail (g_simple_async_result_get_source_tag (simple) == strigi_stream_read_async);

  count = g_simple_async_result_get_op_res_gssize (simple);

  return count;
}

static void
strigi_stream_class_init (StrigiStreamClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GInputStreamClass *ginputstream_class = G_INPUT_STREAM_CLASS (klass);

  g_type_class_add_private (klass, sizeof (StrigiStreamPrivate));

  gobject_class->finalize = strigi_stream_finalize;
  gobject_class->get_property = strigi_stream_get_property;
  gobject_class->set_property = strigi_stream_set_property;

  ginputstream_class->read_fn = strigi_stream_read;
  ginputstream_class->read_async = strigi_stream_read_async;
  ginputstream_class->read_finish = strigi_stream_read_finish;

  g_object_class_install_property (gobject_class, PROP_CPPOBJ,
    g_param_spec_pointer ("cppobj", "cppobj",
                         "C++ object", (GParamFlags)
                         (G_PARAM_CONSTRUCT_ONLY |
                         G_PARAM_READWRITE)));
}

static void
strigi_stream_init (StrigiStream *stream)
{
  stream->priv = G_TYPE_INSTANCE_GET_PRIVATE (stream, STRIGI_TYPE_STREAM, StrigiStreamPrivate);
  stream->priv->lock = g_mutex_new ();
}

GInputStream*
strigi_stream_new (void *cppobj)
{
  return G_INPUT_STREAM (g_object_new (STRIGI_TYPE_STREAM, "cppobj", cppobj, NULL));
}
