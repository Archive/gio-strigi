/* This file is part of Strigi Desktop Search
 *
 * Copyright (C) 2009 Jos van den Oever <jos@vandenoever.info>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifndef GIOINPUTSTREAM_H
#define GIOINPUTSTREAM_H

#include <strigi/bufferedstream.h>
#include <gio/gio.h>

/**
 * This class wraps a GInputStream in a Strigi::BufferedInputStream.
 **/

// can be used for exporting the class in a library
#define TRACKER_EXPORT

namespace Tracker {

/**
 * Simple wrapper of GInputStream in a Strigi stream.
 * This enables analyzing gio streams with libstreamanalyzer.
 **/
class TRACKER_EXPORT GioInputStream : public Strigi::BufferedInputStream {
private:
    class Private;
    Private* const p;
    int32_t fillBuffer(char* start, int32_t space);
public:
    /**
     * Construct a new GioInputStream.
     * Note: GioInputStream does not take ownership of @p input.
     **/
    explicit GioInputStream(GInputStream* input);
    ~GioInputStream();
};

} // end namespace Tracker

#endif
